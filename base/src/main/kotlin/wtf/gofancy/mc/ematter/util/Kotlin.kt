@file:JvmName("KotlinUtils")

package wtf.gofancy.mc.ematter.util

fun <T> Any.uncheckedCast(): T {
    @Suppress("UNCHECKED_CAST")
    return this as T
}
