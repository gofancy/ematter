package wtf.gofancy.mc.ematter.features

import net.minecraftforge.registries.RegistryBuilder
import wtf.gofancy.mc.ematter.client.CustomHighlightProvider

interface ModFeature {

    interface ClientFeature {

        val customHighlightProviders: List<CustomHighlightProvider>

        fun registerModelRegistrar()
        fun registerBlockEntityRenderers()
    }

    val client: () -> () -> ClientFeature

    val customRegistryBuilders: List<RegistryBuilder<*>>

    fun registerRegistrars()

    fun registerCapabilities()

    fun registerEventHandlers()

    fun registerNetworkMessages()
}
