@file:JvmName("DistributionUtils")

package wtf.gofancy.mc.ematter.util

import net.minecraftforge.fml.common.FMLCommonHandler

enum class Distribution {
    CLIENT,
    DEDICATED_SERVER;

    companion object {
        val current = if (FMLCommonHandler.instance().side.isClient) CLIENT else DEDICATED_SERVER
    }
}

fun <T> runSided(client: () -> () -> T, server: () -> () -> T) = when (Distribution.current) {
    Distribution.DEDICATED_SERVER -> server()()
    Distribution.CLIENT -> client()()
}

fun <T> onlyOn(distribution: Distribution, block: () -> () -> T?) = when (distribution) {
    Distribution.CLIENT -> runSided(client = block, server = { { null } })
    Distribution.DEDICATED_SERVER -> runSided(client = { { null } }, server = block)
}

fun <T> clientOnly(block: () -> () -> T?) = onlyOn(Distribution.CLIENT, block)
