package wtf.gofancy.mc.ematter

import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.registries.RegistryBuilder

class CustomRegistryRegistrar(private val customRegistryBuilders: List<RegistryBuilder<*>>) {

    @Suppress("Unused", "Unused_Parameter")
    @SubscribeEvent
    fun registerCustomRegistries(event: RegistryEvent.NewRegistry) {
        this.customRegistryBuilders.forEach { it.create() }
    }
}
