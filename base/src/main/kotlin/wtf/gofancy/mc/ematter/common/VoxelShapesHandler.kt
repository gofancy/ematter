@file:JvmName("VoxelShapesHandler")

package wtf.gofancy.mc.ematter.common

import net.minecraft.util.math.AxisAlignedBB

internal interface VolumeBuilder {
    fun box(fromX: Int, fromY: Int, fromZ: Int, toX: Int, toY: Int, toZ: Int)
}

private class VoxelShapesBuilder : VolumeBuilder {
    private val boxes = mutableListOf<AxisAlignedBB>()

    override fun box(fromX: Int, fromY: Int, fromZ: Int, toX: Int, toY: Int, toZ: Int) {
        this.boxes += AxisAlignedBB(fromX.toDouble() / 16.0 , fromY.toDouble() / 16.0, fromZ.toDouble() / 16.0,
                toX.toDouble() / 16.0, toY.toDouble() / 16.0, toZ.toDouble() / 16.0)
    }

    fun toSequence() = this.boxes.asSequence()
}

internal val emptyVolume = AxisAlignedBB(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

internal fun volumes(builder: VolumeBuilder.() -> Unit): Sequence<AxisAlignedBB> = VoxelShapesBuilder().apply(builder).toSequence()
