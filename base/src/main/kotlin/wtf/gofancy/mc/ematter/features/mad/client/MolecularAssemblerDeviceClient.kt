package wtf.gofancy.mc.ematter.features.mad.client

import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.event.ModelRegistryEvent
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.client.registry.ClientRegistry
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import wtf.gofancy.mc.ematter.MOD_ID
import wtf.gofancy.mc.ematter.client.CustomHighlightProvider
import wtf.gofancy.mc.ematter.client.DefaultCustomHighlightProvider
import wtf.gofancy.mc.ematter.features.ModFeature
import wtf.gofancy.mc.ematter.features.mad.MolecularAssemblerDevice
import wtf.gofancy.mc.ematter.features.mad.common.MadBlock
import wtf.gofancy.mc.ematter.features.mad.common.MadBlockEntity
import wtf.gofancy.mc.ematter.features.mad.common.MadTier

object MolecularAssemblerDeviceClient : ModFeature.ClientFeature {

    override val customHighlightProviders: List<CustomHighlightProvider>
        get() = listOf(
            DefaultCustomHighlightProvider({ MolecularAssemblerDevice.block }, MadBlock.volumes)
        )

    override fun registerModelRegistrar() {
        MinecraftForge.EVENT_BUS.apply {
            register(ModelRegistrar)
        }
    }

    override fun registerBlockEntityRenderers() {
        ClientRegistry.bindTileEntitySpecialRenderer(MadBlockEntity::class.java, MadBlockEntityRender())
    }

    private object ModelRegistrar {
        @Suppress("Unused", "Unused_Parameter")
        @SubscribeEvent
        fun onModelRegistration(event: ModelRegistryEvent) {
            MadTier.values().forEach {
                val mrl = ModelResourceLocation(
                    ResourceLocation(MOD_ID, MolecularAssemblerDevice.item.registryName!!.path + "/" + it.getName()),
                    "inventory"
                )
                ModelLoader.setCustomModelResourceLocation(
                    MolecularAssemblerDevice.item,
                    it.targetMeta,
                    mrl
                )
            }
        }
    }
}
