package wtf.gofancy.mc.ematter.util

import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.Loader
import net.minecraftforge.fml.common.LoaderState
import net.minecraftforge.registries.IForgeRegistryEntry
import wtf.gofancy.mc.ematter.MOD_ID

/**
 * Provides a lazy initializer for [IForgeRegistryEntry] objects that takes care of calling
 * [setRegistryName][IForgeRegistryEntry.setRegistryName] when they are first created.
 *
 * The [initializer] can be any function that returns a registry entry, often a constructor reference. The [rl] will
 * be used as is as the registry name for the object returned by [initializer].
 *
 * The lazy initializer first checks the mod loader's state and throws an `IllegalStateException` if the
 * pre-initialization phase has not yet been reached. This is mostly a dev tool to help ensure that all registry objects
 * are only first accessed when they are registered (which is the only thing that happens in this phase).
 *
 * The name stands for **la**zy **r**egistry **o**bject.
 */
@Suppress("SpellCheckingInspection")
// use `in V` to avoid coercing of  `V` to the direct implementer of `IForgeRegistryEntry` (Block instead of MyBlock)
inline fun <reified V : IForgeRegistryEntry<in V>> laro(
    rl: ResourceLocation,
    crossinline initializer: () -> V
): Lazy<V> = lazy {
    if (!Loader.instance().hasReachedState(LoaderState.PREINITIALIZATION)) {
        val type = V::class.qualifiedName ?: "~~UNKNOWN~~"
        val state = Loader.instance().loaderState
        throw IllegalStateException("The registry object $rl of type $type is accessed to early (during $state)!")
    }

    // need to use apply syntax here because the return of `setRegistryName` is `in V`, which is effectively `Any?`
    initializer().apply {
        this.setRegistryName(rl)
    }
}

/**
 * Shortcut for [laro] that uses uses [MOD_ID] and [name] to construct the [ResourceLocation] that will be used as
 * the registry name for the object created by [initializer].
 */
@Suppress("SpellCheckingInspection")
inline fun <reified V : IForgeRegistryEntry<in V>> laro(name: String, crossinline initializer: () -> V): Lazy<V> =
    laro(ResourceLocation(MOD_ID, name), initializer)
