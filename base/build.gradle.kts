import org.jetbrains.kotlin.gradle.dsl.KotlinVersion
import org.jetbrains.kotlin.gradle.internal.ensureParentDirsCreated
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import wtf.gofancy.deployment.versioning.GitVersionProvider

plugins {
    `java-library`

    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.rfg)

    alias(libs.plugins.versioning)
}

group = property("mod.group") as String

val modId = property("mod.modId") as String
val modName = property("mod.modName") as String

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(8)
    }
}

tasks.withType<KotlinCompile>() {
    compilerOptions {
        /*
         * We need to use Kotlin 1.3 as that is what Shadowfact's Forgelin bundles. Compiling with a different version
         * can cause weird runtime behavior / crashes.
         *
         * todo: bundle the Kotlin standard library ourselves and use an up to date version; make sure to relocate the
         *  library in the resulting jar so that it does not interfere with a potential installation of Forgelin
         */
        @Suppress("Deprecation")
        languageVersion.set(KotlinVersion.KOTLIN_1_3)
    }
}
// todo: switch to below syntax when upgrading Kotlin Gradle plugin version to 1.9+
//kotlin {
//    compilerOptions {
//        languageVersion.set(KotlinVersion.KOTLIN_1_3)
//    }
//}

minecraft {
    mcVersion = "1.12.2"

    injectedTags.putAll(
        mapOf(
            "MOD_ID" to modId,
            "MOD_NAME" to modName,
            "MOD_VERSION" to "${project.version}",
        )
    )

    // todo generate resource files automatically
}

versioning {
    versionProvider = GitVersionProvider(rootProject.projectDir, listOf("1.12.2"))
}

repositories {
    exclusiveContent {
        forRepository {
            maven {
                name = "CurseMaven"
                url = uri("https://www.cursemaven.com")
            }
        }
        filter {
            includeGroup("curse.maven")
        }
    }

    mavenCentral()
}

dependencies {
    // == kotlin ==
    compileOnly(libs.bundles.kotlin)  // for compiling use direct dependencies on the Kotlin libraries
    runtimeOnly(libs.mods.forgelin)   // at runtime use the Kotlin libraries provided by Forgelin

    // == mods useful for testing
    runtimeOnly(libs.mods.jei)

    // == test dependencies ==
    testImplementation(libs.junit.jupiter)
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks.injectTags.configure {
    outputClassName = "${project.group}.generated.InjectedTags"
}

val generateResourcesTask = tasks.register("generateResources") {
    group = "modded minecraft"
    description = "Generates a mcmod.info and pack.mcmeta file"

    // this directory will be used as the resource root
    val resourceRootDir = project.layout.buildDirectory.dir("generated/info")
    outputs.dir(resourceRootDir)

    val mcmodInfoFile = resourceRootDir.map { it.file("mcmod.info") }
    val packMcmetaFile = resourceRootDir.map { it.file("pack.mcmeta") }

    doLast {
        mcmodInfoFile.get().asFile.ensureParentDirsCreated()
        packMcmetaFile.get().asFile.ensureParentDirsCreated()

        val description = project.property("mod.description")
        val website = project.property("mod.website")
        val authors = project.property("mod.authors")
            .toString()
            .split(',')
            .joinToString(prefix = "\"", postfix = "\"", transform = String::trim)
        val credits = project.property("mod.credits")

        mcmodInfoFile.get().asFile.writeText(
            """
            [{
                "modid": "$modId",
                "name": "$modName",
                "description": "$description",
                "version": "${project.version}",
                "mcversion": "1.12.2",
                "logoFile": "",
                "url": "$website",
                "authorList": [$authors],
                "credits": "$credits"
            }]
            """.trimIndent()
        )
        packMcmetaFile.get().asFile.writeText(
            """
            {
                "pack": {
                    "pack_format": 3,
                    "description": "$modName Resource Pack"
                }
            }
            """.trimIndent()
        )
    }
}

sourceSets.main.configure {
    output.dir(generateResourcesTask)
}
